package com.scand.groovy.task1

import org.junit.experimental.categories.Category
import spock.lang.Shared
import spock.lang.Specification

/**
 * Created by kravchenya on 11/16/2018.
 */
@Category(UnitTest.class)
class NumSpec extends Specification {

    @Shared
    List<Integer> numbers1, numbers2, numbers3

    void setupSpec() {
        numbers1 = [56, 13, 45, 45, 321, 1, 48, 56, -1, 83, -36]
        numbers2 = [0, -25, 16, 49, -25, 24, 48]
        numbers3 = [1, 2, 3, 4, 5, 1, 2, 3, 4, 5]
    }

    void "Task 1.2.1: a list of different numbers must be returned"() {
        expect:
        Num.getDifferentNumbersCount(numbers) == expected

        where:
        numbers  | expected
        numbers1 | 9
        numbers2 | 6
        numbers3 | 5
    }

    void "Task 1.2.2: must be returned with a list of maximum and minimum numbers"() {
        expect:
        Num.findMaxMin(numbers) == expected

        where:
        numbers  | expected
        numbers1 | [321, -36]
        numbers2 | [49, -25]
        numbers3 | [5, 1]
    }

    void "Task 1.2.3: each positive number should be doubled, and negative - three times"() {
        expect:
        Num.convert(numbers) == expected

        where:
        numbers  | expected
        numbers1 | [112, 26, 90, 90, 642, 2, 96, 112, -3, 166, -108]
        numbers2 | [0, -75, 32, 98, -75, 48, 96]
        numbers3 | [2, 4, 6, 8, 10, 2, 4, 6, 8, 10]
    }

    void "Tasks 1.2.1-1.2.3: for empty list or null or contains null must be returned null"() {
        expect:
        Num.getDifferentNumbersCount(numbers) == expected
        Num.findMaxMin(numbers) == expected
        Num.convert(numbers) == expected

        where:
        numbers         | expected
        []              | null
        null            | null
        [1, 5, null, 7] | null
    }

    void "Task 1.2.4: the intersection of two arrays must be returned"() {
        expect:
        Num.findNumbersMatches(nums1, nums2) == expected

        where:
        nums1    | nums2    | expected
        numbers1 | numbers2 | [48]
        numbers1 | numbers3 | [1, 1]
        numbers2 | numbers3 | []
    }

    void "Task 1.2.4: for empty lists or null or contains null must be returned null"() {
        expect:
        Num.findNumbersMatches(nums1, nums2) == expected

        where:
        nums1    | nums2                                               | expected
        numbers1 | null                                                | null
        numbers1 | [56, 13, 45, 45, 321, 1, null, 48, 56, -1, 83, -36] | null
        []       | numbers2                                            | null
        numbers3 | [1, 2, 3, 4, 5, 1, 2, 3, 4, 5, null]                | null
    }
}
