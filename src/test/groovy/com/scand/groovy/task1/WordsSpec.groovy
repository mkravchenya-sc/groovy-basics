package com.scand.groovy.task1

import org.junit.experimental.categories.Category
import spock.lang.Specification

/**
*  Created by kravchenya on 11/15/2018.
*/
@Category(UnitTest.class)
class WordsSpec extends Specification {

    void "Task 1.1.1: all words in the string containing only lowercase letters must be found"() {
        expect:
        Words.findAllWordsInLowerCase(string) == expected

        where:
        string                   | expected
        "It's a some big string" | ["a", "some", "big", "string"]
        "Another string"         | ["string"]
        "Some else String"       | ["else"]
    }

    void "Task 1.1.2: must be received characters from 11 to 16"() {
        expect:
        Words.getCharactersFrom11to16(string) == expected

        where:
        string                   | expected
        "0123456789abcdefgh"     | ["a", "b", "c", "d", "e", "f"]
        "It's a some big string" | ["e", " ", "b", "i", "g", " "]
    }

    void "Task 1.1.2: for a string less than 16 characters, null should be returned"() {
        expect:
        Words.getCharactersFrom11to16(string) == expected

        where:
        string       | expected
        "0123456789" | null
        "abcfd"      | null
    }

    void "Task 1.1.3: all english characters must be received"() {
        expect:
        Words.getAllEngCharsInLowerCase(string) == expected

        where:
        string                    | expected
        "Это some большая String" | ["s", "o", "m", "e", "t", "r", "i", "n", "g"]
        "Some String"             | ["o", "m", "e", "t", "r", "i", "n", "g"]
    }

    void "Task 1.1.3: empty list for string win all non-english characters must be received"() {
        expect:
        Words.getAllEngCharsInLowerCase(string) == expected

        where:
        string                  | expected
        "Это не большая строка" | []
        "123 321 456 654"       | []
        "/*-#^%)("              | []
    }

    void "Task 1.1.4: in all words of the line the first letter should be capitalized"() {
        expect:
        Words.capitalizeFirstCharacterInAllWordsA(string) == expected
        Words.capitalizeFirstCharacterInAllWordsB(string) == expected

        where:
        string            | expected
        "hhello a"        | "Hhello A"
        "sOmE string"     | "SOmE String"
        "какая-то Строка" | "Какая-то Строка"
    }

    void "Task 1.1.5: for the string should return a list of palindromes"() {
        expect:
        Words.findPalindromes(string) == expected

        where:
        string                        | expected
        "sdfggfds sdfghjjhgfds dgdgs" | ["sdfggfds"]
        "bob reviver rotator"         | ["bob", "reviver", "rotator"]
        "pop music has palindrome"    | ["pop"]
    }

    void "Task 1.1.5: for the string without palindrome should return an empty list"() {
        expect:
        Words.findPalindromes(string) == expected

        where:
        string                           | expected
        "this string hasn't palindromes" | []
    }

    void "Task 1.1.5: for the string with palindromes GE than 5 chars should return an empty list"() {
        expect:
        Words.findPalindromes(string) == expected

        where:
        string                           | expected
        "this string has big palindrome bigpalindromeemordnilapgib" | []
    }

    void "Task 1.1.6: the string must contain an email address"() {
        expect:
        Words.containsEmail(string)

        where:
        string << [
                "It's a some big string - stepchik@gmai.com",
                "vovchik@mail.ru",
                "This string contains email@some.com"
        ]
    }

    void "Task 1.1.6: the string must not contain an email address"() {
        expect:
        !Words.containsEmail(string)

        where:
        string << [
                "It's a simple string",
                "This string don't contain an e-mail@",
                "This string@ is also.com without an e-mail"
        ]
    }

    void "Task 1.1.6: the string must contain an invalid email address"() {
        expect:
        !Words.containsEmail(string)

        where:
        string << ["stepchik@gmaicom", "vovchik@.ru", "email@some.", "@gmaicom", "stepchik@@gmaicom"]
    }

    void "Tasks 1.1.1-1.1.5: for empty string and null must be returned null"() {
        expect:
        Words.findAllWordsInLowerCase(string) == expected
        Words.getCharactersFrom11to16(string) == expected
        Words.getAllEngCharsInLowerCase(string) == expected
        Words.capitalizeFirstCharacterInAllWordsA(string) == expected
        Words.capitalizeFirstCharacterInAllWordsB(string) == expected
        Words.findPalindromes(string) == expected
        !Words.containsEmail(string)

        where:
        string | expected
        ''     | null
        null   | null
    }


}
