package com.scand.groovy.task7

import com.scand.groovy.task4.Address
import com.scand.groovy.task4.Person

class Task7 {

    static void runTask7_1() {
        println '            * Task 7.1 *\n'

        // 7.1. Операторы:

        // Возвести число 4 в 5 степень;

        println "4 to 5 degrees equals: ${4**5}"
        println()

        /*Написать closure printValue,которое если аргумент не null выводит его значение,
        иначе выводит 'no value'(без оператора 'if')*/

        Closure printValue = { value ->
            println value ?: 'no value'
        }

        println 'Argument 4:'
        printValue(4)
        println 'Argument null:'
        printValue(null)

        println()

        /*У каждого объекта Person вывести значение index (свойство адреса),
        учитывая что у некоторых объектов Person адреса нету (без оператора 'if')*/

        List<Person> persons = [
                new Person(
                        firstName: 'Stepan',
                        surName: 'Stepanov',
                        address: new Address(city: 'Minsk', street: 'Belskiy', index: 111111),
                        age: 27
                ),
                new Person(
                        firstName: 'Vladimir',
                        surName: 'Vladimirov',
                        age: 32
                ),
                new Person(
                        firstName: 'Ivan',
                        surName: 'Ivanov',
                        address: new Address(city: 'Minsk', street: 'Belskiy', index: 1112211),
                        age: 24
                ),
                new Person(
                        firstName: 'Pavel',
                        surName: 'Pavlov',
                        age: 45
                )
        ]

        persons.each { person ->
            Integer index = person?.address?.index
            println "For ${person} index: ${index ?: 'not determined'}"
        }

        println()
    }
}
