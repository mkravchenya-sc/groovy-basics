package com.scand.groovy.task1

/**
*  Created by kravchenya on 11/1/2018.
*/
class Words {

    final static List<String> ALPHABET = 'a'..'z'

    static List<String> findAllWordsInLowerCase(String string) {
        string ? string.tokenize().findAll { it == it.toLowerCase() } : null
    }

    static List<String> getCharactersFrom11to16(String string) {
        string && string.length() > 16 ? string.substring(10, 16).collect { it } : null
    }

    static List<String> getAllEngCharsInLowerCase(String string) {
        string ? string.findAll { ALPHABET.contains(it) } : null
    }

    static String capitalizeFirstCharacterInAllWordsA(String string) {
        if (string) {
            List<String> words = string.tokenize()
            String changingString = ''
            words.eachWithIndex { word, index ->
                changingString += word.replaceFirst(word.substring(0, 1), word.substring(0, 1).toUpperCase())
                if (index != words.size() - 1) {
                    changingString += ' '
                }
            }
            return changingString
        } else {
            return null
        }
    }

    static String capitalizeFirstCharacterInAllWordsB(String string) {
        string ? string.tokenize().collect { word -> word.capitalize() }.join(' ') : null
    }

    static List<String> findPalindromes(String string) {
        string ? string.tokenize().findAll { getUniqueLettersCount(it) < 5 && it == it.reverse() } : null
    }

    static int getUniqueLettersCount(String word) {
        word ? word.collect { it }.unique().size() : null
    }

    static boolean containsEmail(String string) {
        string ? string.tokenize().any { it =~ /\w+@\w+\.\w{2,}/ } : null
    }
}
