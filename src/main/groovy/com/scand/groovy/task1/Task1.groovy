package com.scand.groovy.task1

/**
 * Created by kravchenya on 11/8/2018.
 */
class Task1 {

    static void runTask1_1() {
        println '            * Task 1.1 *\n'

        // 1.1 Дана строка, состоящая из слов

        // 1.1.1 найти все слова, содержащие только буквы нижнего регистра
        String string1 = "It's a some big string"
        println '1.1.1'
        println "All words containing only lowercase letters for string: '${string1}'"
        println Words.findAllWordsInLowerCase(string1)
        println()

        // 1.1.2 вывести символы с 11-го по 16-ый (с учетом, что строка больше 16 символов)
        String string2 = '0123456789abcdefgh'
        println '1.1.2'
        println "Characters from the 11th to the 16th for string: '${string2}'"
        println Words.getCharactersFrom11to16(string2)
        println()

        // 1.1.3 вывести все буквы английского алфавита (нижний регистр)
        String string3 = 'Это some большая String'
        println '1.1.3'
        println "All characters of english alphabet for string: '${string3}'"
        println Words.getAllEngCharsInLowerCase(string3)
        println()

        // 1.1.4 поменять во всех словах строки первую букву на заглавную
        String string4 = 'hhello a'
        println '1.1.4'
        println "Replacement first character to Capital in all words for string: '${string4}'"
        println "a) ${Words.capitalizeFirstCharacterInAllWordsA(string4)}"
        println "b) ${Words.capitalizeFirstCharacterInAllWordsB(string4)}"
        println()

        /* 1.1.5 найти все слова, реверсия которых (обратный порядок букв) идентична самому слову
        и количество различных букв (исключая повторения) в слове меньше 5. (пример: 'bob', 'reriver', 'rotator') */
        String string5 = 'sdfggfds sdfghjjhgfds dgdgs'
        println '1.1.5'
        println "All palindromes which have different letters less than 5 for string: '${string5}'"
        println Words.findPalindromes(string5)
        println()

        // 1.1.6 определить, содержит дли строка электронный адрес
        String string6 = "It's a some big string - stepchik@gmai.com"
        println '1.1.6'
        println "Does the string '${string6}' contain an email address?"
        println Words.containsEmail(string6)
        println()
    }

    static void runTask1_2() {
        println '            * Task 1.2 *\n'

        // 1.2 Дан неупорядоченый массив чисел
        List<Integer> numbers = [56, 13, 45, 45, 321, 1, 48, 56, -1, 83, -36]

        // 1.2.1 найти количество различных чисел
        println '1.2.1'
        println "Count of different numbers for numbers ${numbers}:"
        println "${Num.getDifferentNumbersCount(numbers)}"
        println()

        // 1.2.2 найти максимальное и минимальное число
        println '1.2.2'
        int[] maxMin = Num.findMaxMin(numbers)
        println "For numbers ${numbers} max value: ${maxMin[0]}, min value: ${maxMin[1]}"
        println()

        // 1.2.3 увеличить каждое число в 2 раза если оно положительное и в три раза если оно отрицательное
        println '1.2.3'
        println "Converted array of numbers: ${Num.convert(numbers)}"
        println()

        // 1.2.4 имеется второй массив неупорядоченных чисел: определить числа, входящие и в первый и во второй массив
        println '1.2.4'
        List<Integer> numbers2 = [28, -43, -13, 56, -121, 83]
        println "First array of numbers: ${numbers}"
        println "Second array of numbers: ${numbers2}"
        println "The following matches were found: ${Num.findNumbersMatches(numbers, numbers2)}"
        println()
    }
}
