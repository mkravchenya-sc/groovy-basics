package com.scand.groovy.task1

/**
 * Created by kravchenya on 11/2/2018.
 */
class Num {

    static Integer getDifferentNumbersCount(List<Integer> numbers) {
        numbers && !numbers.contains(null) ? numbers.unique(false).size() : null
    }

    static List<Integer> findMaxMin(List<Integer> numbers) {
        numbers && !numbers.contains(null) ?[numbers.max(), numbers.min()] : null
    }

    static List<Integer> convert(List<Integer> numbers) {
        numbers && !numbers.contains(null) ? numbers.collect { it > 0 ? it * 2 : it * 3 } : null
    }

    static List<Integer> findNumbersMatches(List<Integer> numbers1, List<Integer> numbers2) {
        (numbers1 && numbers2) && (!numbers1.contains(null) && !numbers2.contains(null)) ? numbers1.intersect(numbers2) : null
    }
}
