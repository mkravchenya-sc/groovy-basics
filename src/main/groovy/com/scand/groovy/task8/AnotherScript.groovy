package com.scand.groovy.task8

static void printSeveralTimes(String text, int count) {
    if (!text) {
        println "Invalid text"
        return
    }
    if (count > 1) {
        (1..count).each { println text }
    } else {
        println text
    }
}