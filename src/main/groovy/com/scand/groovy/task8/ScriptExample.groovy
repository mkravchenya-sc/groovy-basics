package com.scand.groovy.task8

import com.scand.groovy.task4.Address
import com.scand.groovy.task4.Person
import groovy.transform.SourceURI

import java.nio.file.Path
import java.nio.file.Paths

println '            * Task 8.1 *\n'

/*Запуск Скриптов
  -Запуск скрипта
  -Компиляция скриптов и их запуск, сравнение производительности
  -Передача параметров
  -Import других классов в скрипт*/

println "It's example of groovy script\n"

@SourceURI
URI sourceUri
Path scriptLocation = Paths.get(sourceUri)

Class clazz = new GroovyClassLoader().parseClass(new File(scriptLocation.parent.toString(), 'AnotherScript.groovy'))
clazz.printSeveralTimes("It's another groovy script", 3)
println()

File scriptFile = new File(getClass().getClassLoader().getResource('ScriptToRun.groovy').file)

run(scriptFile, '2' , '4')
println()

Binding binding = new Binding('5', '7')
GroovyShell shell = new GroovyShell(binding)
shell.evaluate(scriptFile)
println()

binding.variables['string'] = 'some string'
shell.evaluate(scriptFile)
println()

Script script = new GroovyShell().parse(scriptFile)
script.binding.variables['args'] = ['3', '8']
script.binding.variables['string'] = 'Else string'
script.binding.variables['another string'] = 'Another string'
script.run()
println()

Person person = new Person(
        firstName: 'Petr',
        surName: 'Petrov',
        address: new Address(city: 'Vitebsk', street: 'Victory', index: 333333),
        age: 31
)

println person
println()