package com.scand.groovy.task6

import groovy.io.FileType

class Task6 {

    final static String DIRECTORY_PATH = 'src/main/resources/task6'

    static void main(String[] args) {
        println createTestFiles() ? 'Files created successfully!' : "Some files wasn't created"
    }

    static boolean createTestFiles() {
        Map<String, Integer> countProperFilesInDir = [:]
        countProperFilesInDir[DIRECTORY_PATH] = (Integer) Math.random() * 10
        int subfoldersCount = (Integer) Math.random() * 10
        if (subfoldersCount > 0) {
            (1..subfoldersCount).each { index ->
                countProperFilesInDir["${DIRECTORY_PATH}/subfolder${index}/"] = (Integer) Math.random() * 10
            }
        }

        int totalProperFilesCount = countProperFilesInDir.values().sum() as int
        int createdFilesCount = countProperFilesInDir.collect { dir, properFilesCount ->
            new File(dir.toString()).mkdirs()
            properFilesCount > 0 ? createFilesAndGetTheirCount(dir.toString(), properFilesCount) : 0
        }.sum() as int

        println "properFilesCount = ${totalProperFilesCount}, createdFilesCount = ${createdFilesCount}"
        totalProperFilesCount == createdFilesCount
    }

    static int createFilesAndGetTheirCount(String dirPath, int properFilesCount) {
        (1..properFilesCount).findAll { index ->
            new File("${dirPath}/text${index}.txt").createNewFile()
        }.size()
    }

    static void runTask6_1() {
        println '            * Task 6.1 *\n'

        // 6.1. Написать метод( или кложе), который делает рекурсивное удаление всех вложенных файлов (не директории!).

        int totalFilesCount = 0
        int deletedFilesCount = 0

        new File(DIRECTORY_PATH).eachFileRecurse(FileType.FILES, { file ->
            if (file.delete()) {
                deletedFilesCount++
                if (deletedFilesCount == 1) {
                    println "The following files deleted:"
                }
                println "${file.canonicalPath}"
            }
            totalFilesCount++
        })

        println totalFilesCount ?
                (totalFilesCount == deletedFilesCount ? 'All files deleted successfully!' : "All files couldn't deleted") :
                'The folder is empty'

        println()
    }
}
