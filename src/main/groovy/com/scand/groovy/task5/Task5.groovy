package com.scand.groovy.task5

class Task5 {

    final static int TIME_SLEEP = 100
    final static int THREADS_COUNT = 10
    final static int NUMBER_CHANGING_COUNT_FOR_THREAD = 5

    static void runTask5_1() {
        println '            * Task 5.1 *\n'

        /*5.1. Есть некоторая переменная, например Integer number = 0;
        написать запуск нескольких процессов (стартуем к примеру 10 потоков подряд), каждый из которых
        пробует доступиться к этой переменной, увеличить её значение на 1 и затем засыпает на 100 млсекунд.*/

        Integer number = 0

        List<Thread> threads = []

        (1..THREADS_COUNT).each { index ->
            Thread thread = new Thread()
            threads.push(thread)
            thread.start {
                (1..NUMBER_CHANGING_COUNT_FOR_THREAD).each {
                    synchronized (this) {
                        number++
                        println "${thread.name} changes the number to ${number}"
                    }
                    Thread.sleep(TIME_SLEEP)
                }
            }
        }

        Thread.sleep(TIME_SLEEP * (THREADS_COUNT + 1))
        threads.each {
            it.join()
        }

        println()
    }
}
