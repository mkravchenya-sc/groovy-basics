package com.scand.groovy.task4

/**
 * Created by kravchenya on 11/9/2018.
 */
class Person {
    String firstName
    String surName
    Address address
    int age

    Person plus(int age) {
        return new Person(firstName: firstName, surName: surName, address: address, age: this.age + age)
    }

    Person plus(Person person) {
        if (!person) {
            return new Person(firstName: firstName, surName: surName, address: address, age: age)
        }
        String newFirstName = firstName && person.firstName ?
                "${firstName}-${person.firstName}" :
                firstName ?: person.firstName
        String newSurName = surName && person.surName ?
                "${surName}-${person.surName}" :
                surName ?: person.surName
        Address newAddress = address && person.address ?
                address + person.address :
                address ?: person.address
        int newAge = age + person.age
        return new Person(firstName: newFirstName, surName: newSurName, address: newAddress, age: newAge)
    }

    Person minus(int age) {
        int newAge = this.age - age >= 0 ? this.age - age : 0
        return new Person(
                firstName: firstName,
                surName: surName,
                address: new Address(
                        city: address?.city,
                        street: address?.street,
                        index: address ? address.index : 0
                ),
                age: newAge
        )
    }

    Person minus(Person person) {
        if (!person) {
            return new Person(firstName: firstName, surName: surName, address: address, age: age)
        }
        String newFirstName = firstName && person.firstName ?
                (firstName.contains(person.firstName) ? firstName - person.firstName : firstName) :
                (firstName ?: person.firstName)
        String newSurName = surName && person.surName ?
                (surName.contains(person.surName) ? surName - person.surName : surName) :
                (surName ?: person.surName)
        Address newAddress = address && person.address ?
                address - person.address :
                address ?: person.address
        int newAge = age - person.age
        return new Person(firstName: newFirstName, surName: newSurName, address: newAddress, age: newAge)
    }

    String toString() {
        "${firstName} ${surName}"
    }
}
