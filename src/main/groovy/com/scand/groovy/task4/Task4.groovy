package com.scand.groovy.task4

/**
 * Created by kravchenya on 11/8/2018.
 */
class Task4 {

    static void main(String[] args) {
        def address = new Address(index: 100, city: null, street: null)
        def map = [:]
        map[address] = "test"
        println "map = ${map}"
    }

    static void runTask4_1() {
        println '            * Task 4.1 *\n'

        /*4.1. Добавить к классу Integer и интерфейсу List несколько методов (один из них должен быть статическим),
        используя возможности метапрограммирования в Groovy*/

        Integer.metaClass.printThisNum << { ->
            println intValue()
        }

        Integer.metaClass.static.doubleSum << { int num ->
            (1..num).sum() * 2
        }

        Integer integer = new Integer(3)
        println 'The result of the execution of a non-static method added to the class Integer:'
        integer.printThisNum()
        println 'The result of the execution of a static method added to the class Integer:'
        println Integer.doubleSum(4)

        println 'The result of executing a static method added to the class Integer using categories:'
        use(IntegerCategory) {
            println 3.division(2)
        }

        List.metaClass.average << { ->
            int sum = 0
            boolean isAllInteger = delegate.every {
                if (it instanceof Integer) {
                    sum += it
                    return true
                } else {
                    return false
                }

            }
            isAllInteger ? sum / delegate.size() : null
        }

        List.metaClass.static.getIntegerListWithRandomValues << { int size ->
            if (size <= 0) {
                println 'Size must be greater than 0'
                return null
            }
            (0..size).collect { (int) Math.random() * size }
        }

        println 'The result of the execution of a non-static method added to the class List:'
        println([1, 5, 456, 37, 51].average())
        println([1, 32, 'sd', 'sdf', 'sdfaf'].average())
        println 'The result of the execution of a static method added to the class List:'
        println List.getIntegerListWithRandomValues(10)

        println()
    }

    static void runTask4_2() {
        println '            * Task 4.2 *\n'

        /*4.2. Задать объект, типа Closure, который принимает несколько параметров (Integer, String, Closure).
        Затем вызвать closure явным и неявным образом.*/

        Closure closure = { Integer age, String name, Closure callback ->
            callback(age, name)
        }

        println "It's implicit call of closure:"
        closure(27, 'Misha', { Integer age, String name ->
            println "Hello! My name is ${name}, I'm ${age} years old."
        })

        println()

        println "It's explicit call of closure:"
        closure.call(27, 'Misha', { Integer age, String name ->
            println "Hello! My name is ${name}, I'm ${age} years old."
        })

        println()
    }

    static void runTask4_3() {
        println '            * Task 4.3 *\n'

        /*4.3. Написать класс Person (с некоторым набором свойств,
        к примеру firstName, surName, address (city, street, index) , age);

        Определить для объектов класса методы '+', '-';
        Определить метод toString, который выводит "${firstName}, ${surName}"
        Среди объектов типа Person найти такие, у которых возраст (age) менее 30 лет;
        Вывести все различные адреса, которые есть у списка объектов Person;
        Переопределить метод toString, не меняя описания класса Person
        (к примеру теперь пусть выводит "${surName} ${firstName} (${age})").*/

        Person person1 = new Person(
                firstName: 'Petr',
                surName: 'Petrov',
                address: new Address(city: 'Vitebsk', street: 'Victory', index: 333333),
                age: 31
        )

        Person person2 = new Person(
                firstName: 'Petr',
                surName: 'Stepanov',
                address: new Address(city: 'Minsk', street: 'Belskiy', index: 111111),
                age: 27
        )

        println "Person age: ${person1.age} years"
        println("Person age after addition 25 years: ${(person1 + 25).age} years")
        println("Person age after subtraction 10 years: ${(person1 - 10).age} years")
        println("Person age after subtraction more than person has: ${(person1 - 100).age} years")
        println()
        println "Person after addition another person: ${person1 + person2}"
        println()
        println "Person after subtraction another person: ${person1 - person2}"
        println()
        println "Result of toString() method: ${person1}"
        println()

        List<Person> persons = [
                new Person(
                        firstName: 'Stepan',
                        surName: 'Stepanov',
                        address: new Address(city: 'Minsk', street: 'Belskiy', index: 111111),
                        age: 27
                ),
                new Person(
                        firstName: 'Vladimir',
                        surName: 'Vladimirov',
                        address: new Address(city: 'Brest', street: 'Belskiy', index: 111111),
                        age: 32
                ),
                new Person(
                        firstName: 'Ivan',
                        surName: 'Ivanov',
                        address: new Address(city: 'Minsk', street: 'Belskiy', index: 1112211),
                        age: 24
                ),
                null,
                new Person(
                        firstName: 'Pavel',
                        surName: 'Pavlov',
                        address: new Address(city: 'Minsk', street: 'Belskiy', index: 111111),
                        age: 45
                )
        ]

        println "List of persons: ${persons}"
        println "Persons with age less than 30 years: ${persons.findAll { it && it.age < 30 }}"

        print "List of different addresses of persons: "
        println persons.findAll { it?.address }.unique()

        Person.metaClass.toString = { ->
            "${delegate.surName} ${delegate.firstName} (${delegate.age})"
        }

        Person person3 = new Person(firstName: 'Petr', surName: 'Petrov', address: new Address(city: 'Vitebsk', street: 'Victory', index: 333333), age: 31)
        println "\nResult of toString() method after its changing: ${person3.toString()}"

        println()

        Closure printIt = { Person person ->
            println "person: firstName = ${person.firstName}, city: ${person.address?.city}, age: ${person.age}"
        }
        Person p1 = new Person(firstName: 'x', surName: 'John', address: new Address(city: 'Minsk'), age: 50)
        Person p2 = p1 - 10
        printIt(p1)
        printIt(p2)
        p1.address.city = 'Berlin'
        printIt(p1)
        printIt(p2)

        println()
    }
}
