package com.scand.groovy.task4

/**
 * Created by kravchenya on 11/9/2018.
 */
class Address {
    String city
    String street
    int index

    String toString() {
        "Address(${city}, ${street}, ${index})"
    }

    Address plus(Address address) {
        if (!address) {
            return new Address(city: city, street: street, index: index)
        }
        String newCity = city && address.city ? ("${city}-${address.city}") : (city ?: address.city)
        String newStreet = street && address.street ? ("${street}-${address.street}") : (street ?: address.street)
        int newIndex = index && address.index ? (index + address.index) : (index ?: address.index)
        return new Address(city: newCity, street: newStreet, index: newIndex)
    }

    Address minus(Address address) {
        if (!address) {
            return new Address(city: city, street: street, index: index)
        }
        String newCity = city && address.city ?
                (city.contains(address.city) ? city - address.city : city) :
                (city ?: address.city ?: 'unknown')
        String newStreet = street && address.street ?
                (street.contains(address.street) ? street - address.street : street) :
                (street ?: address.street ?: 'unknown')
        int newIndex = index + address.index
        return new Address(city: newCity, street: newStreet, index: newIndex)
    }

    boolean equals(o) {
        if (this.is(o)) {
            return true
        }
        if (!o || getClass() != o.class) {
            return false
        }

        Address address = (Address) o

        if (index != address.index || city != address.city || street != address.street) {
            return false
        }

        return true
    }

    int hashCode() {
        int result = 1
        result = city ? city.hashCode() : result
        result = street ? 31 * result + street.hashCode() : 31 * result
        result = 31 * result + index
        return result
    }
}
