package com.scand.groovy.task4

/**
 * Created by kravchenya on 11/6/2018.
 */
@Category(Integer)
class IntegerCategory {

    double division(int denominator) {
        this / denominator
    }
}