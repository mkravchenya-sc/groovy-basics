package com.scand.groovy.task2

/**
 * Created by kravchenya on 11/8/2018.
 */
class Task2 {

    static String resPath = '/task2'

    static void runTask2_1() {
        println '            * Task 2.1 *\n'

        /*2.1 Просканировать заданную папку на диске. Найти все файлы, подпадающие под шаблон:
        название файла (без учета расширения) заканчивается словом 'groovy'.
        Результат поиска представить в виде списка записей и вывести его.

                Запись имеет вид типа:
        *FileName*
        *Quantity of Files(количество файлов в вложенных папках соответствующие введенному шаблону)*
        *Size (in kb)**/

        Files.getNotesWithAllMatchesFiles(resPath).each { note ->
            println "Name: ${note.name}"
            println "Quantity: ${note.quantity}"
            println "Size: ${note.size}"
            println()
        }
    }

    static void runTask2_2() {
        println '            * Task 2.2 *\n'

        /*2.2. Вывести (представить) записи (полученный в результате работы предыдущей задачи)
        в формате XML (see Groovy MarkupBuilder).*/

        println Files.getXMLWithNotes(Files.getNotesWithAllMatchesFiles(resPath))

        println()
    }
}
