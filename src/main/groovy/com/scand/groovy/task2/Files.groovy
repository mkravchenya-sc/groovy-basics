package com.scand.groovy.task2

import groovy.io.FileType
import groovy.xml.MarkupBuilder

/**
 * Created by kravchenya on 11/2/2018.
 */
class Files {

    static List<Map> getNotesWithAllMatchesFiles(String dirPath) {
        Map<String, Map> notes = new HashMap<>()

        new File(getClass().getResource(dirPath).toURI()).eachFileRecurse(FileType.FILES, { file ->
            String name = file.name
            String nameWithoutExtension = name.lastIndexOf('.') == -1 ? name : name.take(name.lastIndexOf('.'))
            if (nameWithoutExtension =~ /^.*groovy$/) {
                double size = file.length() / 1000
                if (notes[name] == null) {
                    notes[name] = [name: name, quantity: 1, size: size]
                } else {
                    notes[name].quantity++
                }
            }
        })
        return notes.values().toList()
    }

    static getXMLWithNotes(List<Map> notesList) {
        StringWriter writer = new StringWriter()
        MarkupBuilder mb = new MarkupBuilder(writer)
        mb.notes {
            notesList.each { currentNote ->
                note() {
                    name(currentNote.name)
                    quantity(currentNote.quantity)
                    size(currentNote.size)
                }
            }
        }
        return writer.toString()
    }
}
