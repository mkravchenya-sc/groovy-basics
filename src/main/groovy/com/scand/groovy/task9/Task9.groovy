package com.scand.groovy.task9

import groovy.xml.MarkupBuilder

import javax.xml.stream.XMLEventReader
import javax.xml.stream.XMLInputFactory
import javax.xml.stream.XMLStreamConstants
import javax.xml.stream.XMLStreamReader
import javax.xml.stream.events.XMLEvent
import java.time.Duration
import java.time.Instant

class Task9 {

    static void main(String[] args) {
        createBigXML('src/main/resources/task9/', 'big.xml')
    }

    static createBigXML(String dirPath, String fileName) {
        new File(dirPath).mkdirs()
        File bigXML = new File(dirPath + fileName)
        bigXML.createNewFile()
        MarkupBuilder mb = new MarkupBuilder(bigXML.newPrintWriter())

        mb.xml {
            (1..20_000_000).each { index ->
                field('name': "SOME NAME ${index}", "SOME Value ${index}") {

                }
            }
        }

        println bigXML.size()
    }

    static int getCountMatchesFields_XmlParser(String xmlPath, String pattern) {
        new XmlParser().parse(new File(xmlPath)).findAll { node ->
            node.name() == 'field' && node.text() =~ pattern
        }.size()
    }

    static int getCountMatchesFields_XmlSlurper(String xmlPath, String pattern) {
        new XmlSlurper().parse(new File(xmlPath)).'*'.findAll { node ->
            node.name() == 'field' && node.text() =~ pattern
        }.size()
    }

    static int getCountMatchesFields_XMLStreamReader(String xmlPath, String pattern) {
        int matchesCount = 0
        InputStream inputStream = new FileInputStream(xmlPath)
        XMLInputFactory factory = XMLInputFactory.newInstance()
        XMLStreamReader reader = factory.createXMLStreamReader(inputStream)
        try {
            int event
            while (reader.hasNext()) {
                event = reader.next()
                if (event == XMLStreamConstants.START_ELEMENT && reader.name.toString() == 'field') {
                    event = reader.next()
                    if (event == XMLStreamConstants.CHARACTERS && reader.text =~ pattern) {
                        matchesCount++
                    }
                }
            }
        } finally {
            reader.close()
        }
        return matchesCount
    }

    static int getCountMatchesFields_XMLEventReader(String xmlPath, String pattern) {
        int matchesCount = 0
        XMLInputFactory factory = XMLInputFactory.newInstance()
        XMLEventReader reader = factory.createXMLEventReader(new FileInputStream(xmlPath))
        try {
            while (reader.hasNext()) {
                XMLEvent event = reader.nextEvent()
                if (event.isStartElement() && event.asStartElement().name.toString() == 'field') {
                    event = reader.nextEvent()
                    if (event.isCharacters() && event.asCharacters().data =~ pattern) {
                        matchesCount++
                    }
                }
            }
        } finally {
            reader.close()
        }
        return matchesCount
    }

    static void runTask9_1() {
        println '            * Task 9.1 *\n'

        /*9.1. Чтение больших файлов: eсть большой xml файл - 1 GB Структуры
        <xml>
            <field name="SOME NAME">
            SOME Value
            </field>
            .....
	    </xml>
        Необходимо пройтись по нему и найти количество полей (field),
        значение которых соответвствует введенному значению.*/

        println 'Count of fields with a matching values '

        print 'Result for small.xml with using XmlParser: '
        println getCountMatchesFields_XmlParser('src/main/resources/task9/small.xml', /^.*1.*$/)
        print 'Result for small.xml with using XmlSlurper: '
        println getCountMatchesFields_XmlSlurper('src/main/resources/task9/small.xml', /^.*1.*$/)

        print 'Result for big.xml with using XMLEventReader: '
        Instant start = Instant.now()
        println getCountMatchesFields_XMLEventReader('src/main/resources/task9/big.xml', /^.*4563.*$/)
        Instant end = Instant.now()
        println "Duration of the method: ${Duration.between(start, end)}"
        print 'Result for big.xml with using XMLStreamReader: '
        start = Instant.now()
        println getCountMatchesFields_XMLStreamReader('src/main/resources/task9/big.xml', /^.*4563.*$/)
        end = Instant.now()
        println "Duration of the method: ${Duration.between(start, end)}"

        println()
    }
}
