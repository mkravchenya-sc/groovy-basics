package com.scand.groovy.task3

/**
 * Created by kravchenya on 11/2/2018.
 */
class FileChanger {

    static renameFilesIfMatches(String dirPath) {
        if (!dirPath) {
            return 'Wrong directory path'
        }
        String changesFiles = ''
        new File(dirPath).eachFileMatch(~/\d+\.txt/) { file ->
            String nameWithoutExtension = (file.name - '.txt')
            int sum = nameWithoutExtension.collect { it.toInteger() }.sum() as int
            String newName = nameWithoutExtension.reverse() + sum + '.dat'
            if (file.renameTo(file.parent + '/' + newName)) {
                changesFiles += "${file.name} -> ${newName}\n"
            } else {
                println 'No changes'
            }
        }
        changesFiles ? "The following files are renamed:\n${changesFiles}"[0..-2] : 'No matching files'
    }
}
