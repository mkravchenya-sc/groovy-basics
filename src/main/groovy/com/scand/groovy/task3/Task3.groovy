package com.scand.groovy.task3

/**
 * Created by kravchenya on 11/8/2018.
 */
class Task3 {

    static void runTask3_1() {
        println '            * Task 3.1 *\n'

        /*3.1 В заданной папке найти все файлы с расширением TXT, содержащие в названии только цифры:
        для каждого такого файла поменять название на
        [обратный порядок цифр][сумма цифр].dat
        Пример: 12356.txt -> 6532117.dat */

        println FileChanger.renameFilesIfMatches('src/main/resources/task3')

        println()
    }
}
