def printText (){
    println "testText"
}

class EmbeddedObject {

    Integer counter = 0

    String toString() {
        "EmbeddedObject.counter = ${counter}"
    }
}

static int cubesSum(int first, int second) {
    first ** 3 + second ** 3
}

println "Start script execution"
printText()
EmbeddedObject obj = new EmbeddedObject()
obj.counter ++
println "${obj}"

if (args.size() >= 2 && args[0]?.isInteger() && args[1]?.isInteger()) {
    println "Sum cubes of ${args[0]} and ${args[1]} = ${cubesSum(args[0].toInteger(), args[1].toInteger())}"
}

if (binding.hasVariable('string')) {
    println binding.variables['string']
    println binding.variables['another string']
}

println "Script execution is finished"
